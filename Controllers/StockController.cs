using Ex2API.Models;
using Ex2API.Services;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;


namespace StockApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StocksController : ControllerBase
    {
        public readonly DataService _dataService;

        public StocksController(DataService dataservice)
        {
            _dataService = dataservice;
        }

        [HttpGet]
        public async Task<ActionResult<Data>> Get()
        {

            Data dt = new Data();
            DateTimeService datetime = new DateTimeService();
            WeatherService weather = new WeatherService();
            StockService stock_value = new StockService();
            NumberMEaning meaning = new NumberMEaning();
            NewsService Cgi_news = new NewsService();

            dt.DT_Moscow = await datetime.GetMoscowTime();
            dt.DT_Mtl = await datetime.GetMontrealTime();
            dt.SunCloudMtl = await weather.GetWeather();
            dt.CurrentTempMtl = await weather.GetTemp() + "°C";
            dt.SharePrice = await stock_value.GetStockValue() + "$";
            dt.SharePriceChange = stock_value.stockDiff;
            dt.Volume = stock_value.volume;

            dt.Meaning = await meaning.GetNumberMeaning();
            dt.LastNews = await Cgi_news.GetCgiNews();

            List<Data> _data = new List<Data>();
            Data _dt = _dataService.Create(dt);
            _data.Add(new Data()
            {

                Id = _dt.Id,
                DT_Mtl = _dt.DT_Mtl,
                DT_Moscow = _dt.DT_Moscow,
                SunCloudMtl = _dt.SunCloudMtl,
                CurrentTempMtl = _dt.CurrentTempMtl,
                SharePrice = _dt.SharePrice,
                Volume = _dt.Volume,
                Meaning = _dt.Meaning,
                SharePriceChange = _dt.SharePriceChange,
                LastNews = _dt.LastNews

            });

            string json = JsonConvert.SerializeObject(_data.ToArray());

            FileStream stream = new FileStream("./log.json", FileMode.Create);

            using (StreamWriter writer = new StreamWriter(stream, Encoding.UTF8))
                writer.WriteLine(json);

            //System.IO.File.WriteAllText("./log.json", json);

            return Ok(_dt);

        }


        [HttpGet("{id:length(24)}", Name = "GetByID")]
        public ActionResult<Data> Get(string id)
        {
            var stock = _dataService.Get(id);

            if (stock == null)
            {
                return NotFound();
            }

            return stock;
        }


        [HttpGet("{date:length(8)}", Name = "GetByDate")]
        public ActionResult<Data> Get(int date)
        {
            IEnumerable<Data> _date = _dataService.GetDate(date);

            if (_date == null)
            {
                return NotFound();
            }

            return Ok(_date);
        }

        [HttpPost]
        public ActionResult<Data> Create(Data stock)
        {
            _dataService.Create(stock);

            return CreatedAtRoute("GetStock", new { id = stock.Id.ToString() }, stock);
        }
    }

}