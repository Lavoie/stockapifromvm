
namespace Ex2API.Models
{
    public class CgiNews
    {
        public string url { get; set; }
        public string title { get; set; }
        public string summary { get; set; }
        public string date_published { get; set; }

    }
}