using Ex2API.Models;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using System;
namespace Ex2API.Services
{
    public class DataService
    {

        private readonly IMongoCollection<Data> _datas;

        public DataService(IDatabaseSettings settings)
        {
            var client = new MongoClient("mongodb://192.168.18.33:27017");
            var database = client.GetDatabase("admin");
            _datas = database.GetCollection<Data>("stock");
        }

        public List<Data> Get() =>
            _datas.Find(data => true).ToList();

        public Data Get(string id) =>
            _datas.Find<Data>(data => data.Id == id).FirstOrDefault();

        // Date format DDMMYYYY
        public IEnumerable<Data> GetDate(int date)
        {
            DateTime dateT = new DateTime(
                int.Parse(date.ToString().Substring(4, 4)) //y
                ,

                int.Parse(date.ToString().Substring(2, 2)) //m

                ,

                int.Parse(date.ToString().Substring(0, 2)) //d
                );
            var data = from item in _datas.Find(T => true).ToEnumerable()
                       where item.DT_Mtl.DayOfYear == dateT.DayOfYear &&
                       item.DT_Mtl.Year == dateT.Year
                       select item;

            return (data);
        }

        public Data Create(Data data)
        {
            _datas.InsertOne(data);
            return data;
        }

        public void Update(string id, Data dataIn) =>
            _datas.ReplaceOne(data => data.Id == id, dataIn);

        public void Remove(Data dataIn) =>
            _datas.DeleteOne(data => data.Id == dataIn.Id);

        public void Remove(string id) =>
            _datas.DeleteOne(data => data.Id == id);
    }
}