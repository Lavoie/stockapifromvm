using Ex2API.Models;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;

namespace Ex2API.Services
{

    public class StockService
    {
        public string key = "Z188G9ES0UTGOJP5";
        public string stockDiff;
        public string volume;


        public async Task<String> GetStockValue()
        {
            string Url = "https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol=GIB-A.To&apikey=" + key;
            using (HttpClient client = new HttpClient())

            using (HttpResponseMessage res = await client.GetAsync(Url))
            using (HttpContent content = res.Content)
            {
                string data = await content.ReadAsStringAsync();
                if (data != null)
                {
                    JEnumerable<JToken> Stockdata = JObject.Parse(data).Children();

                    List<JToken> tokens = Stockdata.Children().ToList();

                    string _Stockdata = tokens[0].Value<string>("05. price");

                    stockDiff = tokens[0].Value<string>("10. change percent");

                    volume = tokens[0].Value<string>("06. volume");

                    return _Stockdata;
                }
            }
            return null;
        }
    }
}