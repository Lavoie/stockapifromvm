
namespace Ex2API.Services

{
    public class TimerStart
    {
        public static System.Timers.Timer aTimer;


        public static void SetTimer()
        {
            aTimer = new System.Timers.Timer(30000);
            aTimer.Elapsed += TimeLoop.OnTimedEvent;
            aTimer.AutoReset = true;
            aTimer.Enabled = true;
        }


    }
}